<?php

class Question
{
    private $conn;
    private $table_name = "questions";

    public $id;
    public $question;
    public $deleted;

    public function __construct($db)
    {
        $this->conn = $db;
    }

    /*
     * Get questions
     */
    function getAll()
    {

        $query = "SELECT q.id, q.question
        FROM {$this->table_name} as q
        WHERE q.deleted = 0 
        ORDER BY id ASC";

        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        return $stmt;
    }

    function getQuestionAnswers($questionId) {
        $questionId = htmlspecialchars(strip_tags($questionId));

        $query = "SELECT a.id, a.answer, a.correct_answer
                  FROM answers as a
                  WHERE a.question_id = {$questionId}";
        $stmt = $this->conn->prepare($query);
        $stmt->execute();

        $res = [];
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
            $res[] = $row;
        }

        return $res;
    }

}