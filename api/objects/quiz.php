<?php

class Quiz
{
    private $conn;
    private $table_name = "quiz_results";

    public $id;
    public $name;
    public $percentage_correct_answers;

    public function __construct($db)
    {
        $this->conn = $db;
    }

    function create()
    {
        // query to insert record
        $query = "INSERT INTO {$this->table_name} SET name=:name, percentage_correct_answers=:percentage_correct_answers ";

        // prepare query
        $stmt = $this->conn->prepare($query);

        // sanitize
        $this->name = htmlspecialchars(strip_tags($this->name));
        $this->percentage_correct_answers = htmlspecialchars(strip_tags($this->percentage_correct_answers));

        // bind values
        $stmt->bindParam(":name", $this->name);
        $stmt->bindParam(":percentage_correct_answers", $this->percentage_correct_answers);
        // execute query

        if ($stmt->execute()) {
            return true;
        }

        return false;
    }

}