class Quiz {

    constructor(quizSelector, apiRoute) {
        this.quizContainer = document.querySelector(quizSelector);
        this.currentQuestion = 0; // 0 - first
        this.answeredQuestions = {};
        this.apiRoute = apiRoute;
        this.questions = this.getQuestions();
    }

    startQuiz() {
        this.appendQuestion();
    }

    appendQuestion(currentQuestion = this.currentQuestion) {
        this.questions.then((questions) => {
            questions = JSON.parse(questions);

            if ((!questions) || (!questions[currentQuestion])) {
                return;
            }
            this.currentQuestion = currentQuestion;

            const question = questions[currentQuestion];
            const answers = questions[currentQuestion].answers;

            this.quizContainer.querySelector('.question').innerHTML = question.question;

            const asnwersContainer = this.quizContainer.querySelector('.answers');
            asnwersContainer.innerHTML = '';

            Object.values(answers).map((answer) => {
                let checkboxAnswerOption = document.createElement('input');
                checkboxAnswerOption.type = 'checkbox';
                checkboxAnswerOption.value = answer.id;
                checkboxAnswerOption.id = answer.id;
                if (Object.entries(this.answeredQuestions).length > 0) {
                    checkboxAnswerOption.checked = (this.answeredQuestions[this.currentQuestion] && Object.values(this.answeredQuestions[this.currentQuestion]).includes(answer.id)) ? true : false;
                }

                let checkboxValueText = document.createElement('label');
                checkboxValueText.setAttribute('for',checkboxAnswerOption.id);
                checkboxValueText.innerHTML = answer.answer;

                asnwersContainer.appendChild(checkboxAnswerOption)
                asnwersContainer.appendChild(checkboxValueText)
            });
        });
    }

    saveAnswer() {
        const selectedAnswers = this.quizContainer.querySelectorAll('.answers input:checked');
        const selectedAnswersLength = selectedAnswers.length;

        if (selectedAnswersLength > 0) {
            this.answeredQuestions[this.currentQuestion] = [];

            for(let i=0; i<selectedAnswersLength; i++) {
                let answerId = selectedAnswers[i].id;
                this.answeredQuestions[this.currentQuestion].push(answerId);
            }
        }
    }

    handlePreviousQuizQuestion() {
        //Todo check for first/last question and enable/disable the button
        this.appendQuestion(this.currentQuestion-1);
    }

    handleNextQuizQuestion() {
        //Todo check for first/last question and enable/disable the button
        this.saveAnswer();
        this.appendQuestion(this.currentQuestion+1);
    }

    handleSubmitQuiz() {
        this.saveAnswer();
        this.showQuizSummary();
    }

    showQuizSummary() {
        this.questions.then((questions) => {
            questions = JSON.parse(questions);
            const quizSummaryContainer = this.quizContainer.querySelector('.quiz-summary');
            quizSummaryContainer.innerHTML = '';

            let correctQuestionAnswersCount = 0;
            let correctQuestionAnswer;
            let atLeastOneWrongAnswer;

            Object.values(questions).map((question, questionCounter) => {
                correctQuestionAnswer = false;
                atLeastOneWrongAnswer = false;

                let summaryQuestionContainer = document.createElement('p');
                summaryQuestionContainer.innerHTML = 'Question: '+question.question;
                quizSummaryContainer.appendChild(summaryQuestionContainer);
                
                Object.values(question.answers).map((answer) => {
                    if (this.answeredQuestions[questionCounter] && Object.values(this.answeredQuestions[questionCounter]).includes(answer.id)) { //If answer found
                        let answerContainer = document.createElement('div');
                        answerContainer.innerHTML = answer.answer;
                        if (answer.correct_answer == 1) {
                            correctQuestionAnswer = true;
                            answerContainer.classList.add('correct-answer');
                        } else {
                            atLeastOneWrongAnswer = true;
                            answerContainer.classList.add('wrong-answer');
                        }
                        quizSummaryContainer.appendChild(answerContainer);
                    }
                });

                if (correctQuestionAnswer === true && atLeastOneWrongAnswer === false) {
                    correctQuestionAnswersCount++
                }
            });

            // Save the data
            let data = {
                name: 'Denis Kalfov',
                percentage_correct_answers: correctQuestionAnswersCount/questions.length * 100
            }
            this.makeAjaxCall(this.apiRoute + 'quiz/create', 'POST', data);

            
        });
    }

    makeAjaxCall(url, methodType, data = {}) {
        return new Promise((resolve, reject) => {
            $.ajax({
                url: url,
                type: methodType,
                data: data,
                success: function(data) {
                    resolve(data)
                },
                error: function(error) {
                    reject(error)
                },
            })
        })
    }

    getQuestions() {
        const getQuestionsUrl = this.apiRoute + 'question/read';
        return this.makeAjaxCall(getQuestionsUrl, 'GET');
    }

}

const q = new Quiz('.quiz-container', 'http://localhost/php-tasks/MobileWaveSolutions/master/api/');
q.startQuiz();

document.getElementById('previousQuestion').addEventListener('click', () => q.handlePreviousQuizQuestion());
document.getElementById('nextQuestion').addEventListener('click', () => q.handleNextQuizQuestion());
document.getElementById('submitQuiz').addEventListener('click', () => q.handleSubmitQuiz());
