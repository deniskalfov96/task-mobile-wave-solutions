/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : test

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2019-12-21 00:29:52
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for answers
-- ----------------------------
DROP TABLE IF EXISTS `answers`;
CREATE TABLE `answers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question_id` int(11) DEFAULT NULL,
  `answer` varchar(255) DEFAULT NULL,
  `correct_answer` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of answers
-- ----------------------------
INSERT INTO `answers` VALUES ('1', '1', 'Нил', '1');
INSERT INTO `answers` VALUES ('2', '1', 'Амазонка', '0');
INSERT INTO `answers` VALUES ('3', '1', 'Марица', '0');
INSERT INTO `answers` VALUES ('4', '2', '5', '0');
INSERT INTO `answers` VALUES ('5', '2', '25', '0');
INSERT INTO `answers` VALUES ('6', '2', '64', '1');
INSERT INTO `answers` VALUES ('7', '2', '73', '0');
INSERT INTO `answers` VALUES ('8', '2', '82', '0');
INSERT INTO `answers` VALUES ('9', '3', '1+1=2', '1');
INSERT INTO `answers` VALUES ('10', '3', 'pi=6.346623', '0');
INSERT INTO `answers` VALUES ('11', '3', '8134*0=0', '1');
