/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50505
Source Host           : localhost:3306
Source Database       : test

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2019-12-21 00:30:07
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for quiz_results
-- ----------------------------
DROP TABLE IF EXISTS `quiz_results`;
CREATE TABLE `quiz_results` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `percentage_correct_answers` decimal(11,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of quiz_results
-- ----------------------------
INSERT INTO `quiz_results` VALUES ('49', 'Denis Kalfov', '0.00');
INSERT INTO `quiz_results` VALUES ('50', 'Denis Kalfov', '0.00');
INSERT INTO `quiz_results` VALUES ('51', 'Denis Kalfov', '33.33');
INSERT INTO `quiz_results` VALUES ('52', 'Denis Kalfov', '66.67');
INSERT INTO `quiz_results` VALUES ('53', 'Denis Kalfov', '100.00');
