<?php
// required header
header("Access-Control-Allow-Origin: *");

define('ROOT', 'C:/xampp/htdocs/php-tasks/MobileWaveSolutions/master/');

include_once(ROOT . 'api/config/database.php');

$database = new Database();
$db = $database->getConnection();

$page = !empty($_GET['page']) ? $_GET['page'] : "/";

$data = json_decode(file_get_contents("php://input"));
if ($data) {
    $_POST = array_merge($_POST, (array)$data);
}

//print_r($_POST);die;
//echo $page;
$apiFileRoute = ROOT . "api{$page}.php";

// Check if file exists
if (file_exists($apiFileRoute)) {
    require($apiFileRoute);
} else {
    http_response_code(404);
    echo json_encode(array("message" => "Невалиден път."));
}
